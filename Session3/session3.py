import re, glob, os
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import CubicSpline

os.chdir("Session3")
data = [[], [], [], [], [], [], [], [], [], []]

for file in glob.glob("*.txt"):
    with open(file) as f:
        f_num = re.findall(r"\d+", file)
        f_num = int(f_num[0]) - 1
        filedata = []
        while True:
            line = f.readline()
            if not line:
                break
            filedata.append(line)
        for line in filedata:
            d = {"t": 0, "x": 0, "y": 0, "v": 0}
            if re.match(r"([ -]?[01]{1}[.]\d*[E]*[-+]*\d*[\s]){4}", line):
                datapoints = line.split()
                d["t"], d["x"], d["y"], d["v"] = datapoints[-4:]
                data[f_num].append(d)

# print(data[0])
end_speeds = [float(d[-3]["v"]) for d in data]
avg_end_speed = np.average(end_speeds)
# print(end_speeds)
print("Average end speed:", avg_end_speed)
s_deviation = np.sqrt(
    (1)
    / (len(data) - 1)
    * np.sum([(end_speed - avg_end_speed) ** 2 for end_speed in end_speeds])
)
print("Standard deviation in end speed:", s_deviation)
s_error = s_deviation / np.sqrt(len(end_speeds))
print("Standard error:", s_error)


# Plotting
x = [float(data[0][i]["x"]) for i in range(len(data[0]))]
y = [float(data[0][i]["y"]) for i in range(len(data[0]))]
print("x", len(x), "y", len(y))
h = 0.200
baneform = plt.figure('y(x)',figsize=(12,6))
plt.plot(x,y,'.')
plt.title('$y$(x)',fontsize=20)
plt.xlabel('$x$(m)',fontsize=20)
plt.ylabel('$y$(m)',fontsize=20)
plt.ylim(0.0,0.4)
plt.grid()
plt.show()

t = [float(data[0][i]["t"]) for i in range(len(data[0]))]
baneform = plt.figure("x(t)", figsize=(12, 6))
plt.plot(t, x, ".")
plt.title("$x$(t)", fontsize=20)
plt.xlabel("$t$(s)", fontsize=20)
plt.ylabel("$x$(m)", fontsize=20)
plt.grid()
plt.show()
